mysql.local - aes128keyk5m1 
export
keytool -importkeystore -srckeystore encryption.keystore -srcstorepass changeit -srcstoretype jceks -alias aes128keyk5m1 -destkeystore mysql-temp.keystore -deststorepass changeit -deststoretype jceks

import
keytool -importkeystore -srckeystore mysql-temp.keystore -srcstorepass changeit -srcstoretype jceks -alias aes128keyk5m1 -destkeystore encryption.keystore -deststorepass changeit -deststoretype jceks

ucdlab.local - aes128keyo4qq
export
keytool -importkeystore -srckeystore encryption.keystore -srcstorepass changeit -srcstoretype jceks -alias aes128keyo4qq -destkeystore ucdlab-temp.keystore -deststorepass changeit -deststoretype jceks

import
keytool -importkeystore -srckeystore ucdlab-temp.keystore -srcstorepass changeit -srcstoretype jceks -alias aes128keyo4qq -destkeystore encryption.keystore -deststorepass changeit -deststoretype jceks
