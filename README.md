# UCD JPetStore Process Assets

This repository contains the exported applications covering Labs 1 to 25,the source UCD server encryption keystore, and the command line for how to import it into the destination UCD server. This way the lab administrator can install the end state of the lab without having to perform all lab instructions.